$(document).ready(function () {
  $(".owl-carousel").owlCarousel();
});

$('.owl-carousel').owlCarousel({
  loop:true,
  margin:10,
  responsiveClass:true,
  autoWidth:true,
  autoHeight:true,
  center:true,
  responsive:{
      200:{
          items:2,
          nav:false
      },
      400:{
          items:1,
          nav:false
      },
      1200:{
          items:1,
          loop:true,
          margin: 10,
      }
  }
})





