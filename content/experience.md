---
title: "Education"
url: /education
---

# Exhibitions 


# 2019 
#### Bearings
 School of Art Design Media Gallery, Singapore 

#### Street of Clans Festival
 Koh Clan, Singapore

# Education 

#### Nanyang Technological University (2017 - Present) 
School of Art, Design and Media 
Interactive Media Major 

#### Ngee Ann Polytechnic  (2012 - 2015) 
Diploma in Arts Business Management 
Specialization in Visual Arts Management 