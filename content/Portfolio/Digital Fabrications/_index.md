---
title: "Digital Fabrications"
url: "/digitalfabrications"
maxWidth: "400x"
clickablePhotos: false
type: "gallery"
---

A semester experimenting with digital fabrication techniques and concepts in Columbia College Chicago. 

With the help of the enthusiastic and experienced faculty and technicians, I spent the semester understanding the work flow and nuances of designing with Laser machines, 3D printing and CNC mills.  

Using various software and hardwares such as Rhinoceros, Meshmixer and 3D scanners, I created a series of toys that work as analog synthesizers and sound making sculptures. 