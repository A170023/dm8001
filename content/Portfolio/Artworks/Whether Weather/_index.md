---
title: "whether weather"
url: "/whetherweather"
maxWidth: "400x"
clickablePhotos: false
type: "gallery"
---

### 2019 

***whether weather*** is a collection of weather apparatus conceived based on regional weather lore and myths from the united kingdom. The project references Tristan Gooley’s *The Walker’s Guide to Outdoor Clues & Signs*, where he provides scientific logic behind some of the myths.  

Inspired by the tension between relying on scientific weather instruments and our gut intuition of the weather, the artist conceives a set of fictional weather contraptions that are informed both by science and myths. 

The contraptions are accompanied by it’s conceptual sketches and documentation from field tests in the Scottish landscapes.  

The installation is presented as a collection of sculptures and a video of the sculptures on location. 