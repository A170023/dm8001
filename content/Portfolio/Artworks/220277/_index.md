---
title: "220277"
url: "/220277"
type: "gallery"
maxWidth: "400x"
clickablePhotos: true
---

 
### 2018 


***200227***​ is a sound installation that examines the comfort in anticipation, rhythm and repetition. 

The sculpture projects a chaotic rhythm with the pens and solenoids when viewed from a distance. 

When the viewer approaches the work, the pens will start to blend and click in synchronization. 