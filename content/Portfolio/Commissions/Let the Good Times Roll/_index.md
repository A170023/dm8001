---
title: "Let The Good Times Roll"
url: "/LTGTR"
maxWidth: "400x"
clickablePhotos: false
type: "gallery"
---

by To The Power Of  
( Pixie Tan, Ng Fong Yee, Lim Pin Han, Sebastian Wong ) 

2018 

As part of a collaborative model curated by​ ​Pixie Tan​, To The Power Of featured 5 multidisciplinary artists and designers working together to present an interactive installation for the Koh Clan for Street of Clans Festival.  

Tasked to translate the clan value of unity, a crucial story of the Koh Clan, we used the characteristics of a Rube Goldberg machine that required multiple participants to work together to activate the piece.