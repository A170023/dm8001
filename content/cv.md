---
title: "About"
date: 2020-08-04T22:44:56+08:00
draft: false
---
# Experiences
 

#### The Curious Sky by Ng Fong Yee (2019) 

Artist Assistant/Technical Support 


#### Singapore Tyler Print Institute (May - July 2018) 

Paper Room Intern 


#### DECK (Feb - June 2015) 

Darkroom Instructor 


#### Singapore International Photography Festival (Sept 2014 - June 2015) 

Educational & Outreach Intern 

#### A Million Stories of Us by Ang Song Nian (2015) 

Artist Assistant / Project Manager 

#### Analog Film Lab (2012  - 2015) 

Darkroom Lab Assistant 